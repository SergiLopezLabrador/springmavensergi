package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SergiController {

	@RequestMapping("/saludo")
	public String hola() {
		return "SergiWorld";
	}
}
